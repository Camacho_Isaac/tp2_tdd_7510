# 75.10 Técnicas de Diseño - Template project
 ![Build Status](https://travis-ci.org/7510-tecnicas-de-disenio/template.svg?branch=master) 

Template Gradle project

* Gradle
* Checkstyle
* Findbugs
* PMD
* JaCoCo

To run project :
* gradle clean build
* chmod 755 gradlew
* ./gradlew build
* java -jar build/libs/gs-rest-service-0.1.0.jar