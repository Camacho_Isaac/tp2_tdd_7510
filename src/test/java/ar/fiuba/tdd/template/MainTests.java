package ar.fiuba.tdd.template;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MainTests {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    CitizenBuilder citizenbuilder;
    DNIBuilder dniBuilder;
    Tramite tramite;

    @Before
    public void setUp() {
        citizenbuilder = new CitizenBuilder();
        citizenbuilder.addName("Santiago");
        citizenbuilder.addAge(24);
        citizenbuilder.addAddress("San Isidro");
        Citizen ciudadano = citizenbuilder.build();
        dniBuilder = new DNIBuilder(ciudadano);
        dniBuilder.addPhoto("photo");
        dniBuilder.addFingerprint("fingerprint");
        tramite = new Tramite();
    }

    @Test
    public void TestCreacionDeCiudadano() {
        Citizen ciudadano = citizenbuilder.build();
        assertEquals("Santiago", ciudadano.getName());
        assertEquals(24, ciudadano.getAge());
        assertEquals("San Isidro", ciudadano.getAddress());
    }

    @Test
    public void TestImpresionDelDocumento() {
        System.setOut(new PrintStream(outContent));
        DNI dni = dniBuilder.build();
        ProcessDNI processDNI = new ProcessDNI(dni);
        Documento documento = processDNI.generarDocumento();
        Printer printer = new Printer();
        printer.print(documento);
        assertEquals("Name: Santiago, Adress: San Isidro, Age: 24, Photo: photo, Fingerprint: fingerprint", outContent.toString());
        System.setOut(originalOut);
    }

    @Test
    public void TestRegistrarDocumentoParaSerRetirado() {
        DNI dni = dniBuilder.build();
        ProcessDNI processDNI = new ProcessDNI(dni);
        Documento documento = processDNI.generarDocumento();
        Printer printer = new Printer();
        printer.print(documento);
        Correo correo = new Correo();
        documento.sePuedeRetirar(correo);
        assertTrue(documento.esRetirable());
    }

    @Test
    public void TestNotificarAlCorreo(){
        DNI dni = dniBuilder.build();
        ProcessDNI processDNI = new ProcessDNI(dni);
        Documento documento = processDNI.generarDocumento();
        Printer printer = new Printer();
        printer.print(documento);
        Correo correo = new Correo();
        documento.sePuedeRetirar(correo);
        assertTrue(correo.paraEntregar(documento));
    }

    @Test
    public void ActualizarEstadoFinalDelTramite(){
        DNI dni = dniBuilder.build();
        ProcessDNI processDNI = new ProcessDNI(dni);
        Documento documento = processDNI.generarDocumento();
        Printer printer = new Printer();
        printer.print(documento);
        Correo correo = new Correo();
        documento.sePuedeRetirar(correo);
        correo.completarEntrega();
        tramite.finalizar();
        assertEquals(Estado.FINAL,tramite.estado());
    }
}