package ar.fiuba.tdd.template;

public class Printer {
    public Printer(){}

    public void print(Documento documento) {
        String doc = documento.getData();
        try {
            System.out.print(doc);
        }
        catch(Exception ex){
            throw ex;
        }
    }
}
