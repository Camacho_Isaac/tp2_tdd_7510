package ar.fiuba.tdd.template;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
public interface CitizenRepository extends CrudRepository<Citizen,String> {
    List<Citizen> findByName(String name);
}
