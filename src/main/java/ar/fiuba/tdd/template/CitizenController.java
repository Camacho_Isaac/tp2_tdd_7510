package ar.fiuba.tdd.template;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/citizen")
public class CitizenController {
    @Autowired
    CitizenRepository repository;
    @RequestMapping(value="/",method=RequestMethod.POST)
    public String newCitizen(@RequestParam(value="name", defaultValue="") String name,
                             @RequestParam(value="id", defaultValue="") String id,
                            @RequestParam(value="age",defaultValue = "") int age,
                             @RequestParam(value="address",defaultValue = "") String address){
        Citizen c = new CitizenBuilder()
                .addAddress(address)
                .addAge(age)
                .addName(name)
                .addId(id)
                .build();
        repository.save(c);
        return "SAVED CORRECTLY";
    }
    @RequestMapping(value = "/",method= RequestMethod.GET)
    public List<Citizen> searchCitizen(@RequestParam(value="name", defaultValue="") String name){
        if(name == ""){
            return (List<Citizen>) repository.findAll();
        }
        return repository.findByName(name);
    }
    @RequestMapping(value = "/{id}",method= RequestMethod.GET)
    public Citizen getCitizen(@PathVariable int id){
        Citizen c = new Citizen();//replace with db query
        c.setName("Juan");
        c.setAge(20);
        c.setAddress("fafafa 123");
        return c;
    }
}
