package ar.fiuba.tdd.template;

public class DNIBuilder {
    private DNI dni;
    DNIBuilder(Citizen citizen){
        this.dni = new DNI(citizen);
    }
    public void addPhoto(String photo){
        dni.addPhoto(photo);
    }
    public void addFingerprint(String fingerPrint){
        dni.addFingerprint(fingerPrint);
    }
    public DNI build(){
        return this.dni;
    }
}
