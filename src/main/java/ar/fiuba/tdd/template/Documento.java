package ar.fiuba.tdd.template;

public class Documento {
    String data;
    boolean retirable = false;

    public void addData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void sePuedeRetirar(Correo correo) {
        retirable = true;
        correo.agregarEntrega(this);
    }

    public boolean esRetirable() {
        return retirable;
    }
}
