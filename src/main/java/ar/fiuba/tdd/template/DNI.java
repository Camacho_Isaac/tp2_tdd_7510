package ar.fiuba.tdd.template;


import java.util.HashMap;

public class DNI {
    private String name;
    private int age;
    private String adress;
    private String photo;
    private String fingerPrint;
    public DNI(Citizen citizen) {
        name = citizen.getName();
        age = citizen.getAge();
        adress = citizen.getAddress();
    }

    public void addPhoto(String photo) {
        this.photo = photo;
    }

    public void addFingerprint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return adress;
    }

    public int getAge() {
        return age;
    }

    public String getPhoto() {
        return this.photo;
    }

    public String getFingerPrint() {
        return this.fingerPrint;
    }
}
