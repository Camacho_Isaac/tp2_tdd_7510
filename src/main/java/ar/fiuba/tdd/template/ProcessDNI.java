package ar.fiuba.tdd.template;

public class ProcessDNI {
    private final DNI dni;

    public ProcessDNI(DNI dni ){
        this.dni = dni;
    }
    public String issueDNI(){
        return String.format("Name: %s, Adress: %s, Age: %d, Photo: %s, Fingerprint: %s",
                this.dni.getName(),
                this.dni.getAddress(),
                this.dni.getAge(),
                this.dni.getPhoto(),
                this.dni.getFingerPrint());
    }

    public Documento generarDocumento() {
        Documento doc = new Documento();
        StringBuffer buffer = new StringBuffer();
        buffer.append("Name: ");
        buffer.append(this.dni.getName());
        buffer.append(", Adress: ");
        buffer.append(this.dni.getAddress());
        buffer.append(", Age: ");
        buffer.append(this.dni.getAge());
        buffer.append(", Photo: ");
        buffer.append(this.dni.getPhoto());
        buffer.append(", Fingerprint: ");
        buffer.append(this.dni.getFingerPrint());
        doc.addData(buffer.toString());
        return doc;
    }
}
