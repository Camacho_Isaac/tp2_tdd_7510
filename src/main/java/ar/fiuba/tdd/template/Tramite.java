package ar.fiuba.tdd.template;

public class Tramite {
    Estado estado;
    Tramite(){
        estado = Estado.INICIAL;
    }

    public void finalizar() {
        estado = Estado.FINAL;
    }

    public Estado estado() {
        return estado;
    }
}
