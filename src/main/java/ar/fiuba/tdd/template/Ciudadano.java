package ar.fiuba.tdd.template;

public class Ciudadano {
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public long getDni() {
        return Dni;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    private long Dni;
    private String Domicilio;

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDni(long dni) {
        Dni = dni;
    }

    public void setDomicilio(String domicilio) {
        Domicilio = domicilio;
    }
}
