package ar.fiuba.tdd.template;

public class CitizenBuilder {
    private Citizen citizen;
    public CitizenBuilder(){
        this.citizen = new Citizen();
    }
    public CitizenBuilder addName(String name){
        this.citizen.setName(name);
        return this;
    }
    public CitizenBuilder addAddress(String address){
        this.citizen.setAddress(address);
        return this;
    }
    public CitizenBuilder addAge(int age){
        this.citizen.setAge(age);
        return this;
    }
    public CitizenBuilder addId(String id){
        this.citizen.setId(id);
        return this;
    }
    public Citizen build(){
        return this.citizen;
    }
}
