package ar.fiuba.tdd.template;


import java.util.HashSet;

public class Correo {
    private HashSet<Documento> paraEntregar;
    Correo(){
        paraEntregar = new HashSet<Documento>();
    }
    public void agregarEntrega(Documento documento) {
        paraEntregar.add(documento);
    }

    public boolean paraEntregar(Documento documento) {
        return paraEntregar.contains(documento);
    }

    public void completarEntrega() {
    }
}
